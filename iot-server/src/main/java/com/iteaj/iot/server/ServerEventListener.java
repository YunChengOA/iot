package com.iteaj.iot.server;

import com.iteaj.iot.event.IotEvent;
import com.iteaj.iot.event.StatusEventListener;

public interface ServerEventListener extends StatusEventListener<String, SocketServerComponent> {

    @Override
    default boolean isMatcher(IotEvent event) {
        return StatusEventListener.super.isMatcher(event)
                && event.getComponent() instanceof ServerComponent;
    }
}
