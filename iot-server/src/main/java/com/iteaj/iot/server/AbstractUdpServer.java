package com.iteaj.iot.server;

import com.iteaj.iot.PortType;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.channel.ChannelInboundHandlerAdapter;

public abstract class AbstractUdpServer implements IotSocketServer {

    private ConnectProperties config;

    public AbstractUdpServer(ConnectProperties config) {
        this.config = config;
    }

    @Override
    public PortType getPortType() {
        return PortType.Udp;
    }

    @Override
    public ConnectProperties config() {
        return this.config;
    }

    /**
     * 返回设备解码器
     * @return
     */
    public abstract ChannelInboundHandlerAdapter getMessageDecoder();
}
