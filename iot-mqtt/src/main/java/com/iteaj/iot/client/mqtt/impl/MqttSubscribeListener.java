package com.iteaj.iot.client.mqtt.impl;

import io.netty.handler.codec.mqtt.MqttTopicSubscription;

/**
 * mqtt全局订阅监听器
 * @see DefaultMqttComponent Spring容器里面包含监听器对象则默认启用此组件
 */
public interface MqttSubscribeListener extends MqttListener {

    /**
     * 要订阅的topic
     * @see DefaultMqttConnectProperties mqtt broker配置
     * @return 如果返回null, 则将不对此客户端进行订阅
     */
    MqttTopicSubscription topic(DefaultMqttConnectProperties client);

}
